package inf101h22.university;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

public class GradeBookTest {
    
    @Test
    public void gpaTest1() {
        List<Grade> grades = Arrays.asList(Grade.A, Grade.E, Grade.B,
                                            Grade.B, Grade.C);
        double expectedAverage = 3.4;
        GradeBook gpa = new GradeBook(grades);
        assertEquals(expectedAverage, gpa.getGpa());
    }

    @Test
    public void gpaTest2() {
        List<Grade> grades = Arrays.asList(Grade.E, Grade.D, Grade.B,
                                            Grade.E, Grade.FAIL);
        double expectedAverage = 2.0;
        GradeBook gpa = new GradeBook(grades);
        assertEquals(expectedAverage, gpa.getGpa());
    }

    @Test
    public void gpaTest3() {
        List<Grade> grades = Arrays.asList(Grade.FAIL, Grade.FAIL, Grade.FAIL,
                                            Grade.FAIL, Grade.FAIL);
        double expectedAverage = 0.0;
        GradeBook gpa = new GradeBook(grades);
        assertEquals(expectedAverage, gpa.getGpa());
    }

    @Test
    public void gpaTest4() {
        List<Grade> grades = Arrays.asList(Grade.A, Grade.E, Grade.B, Grade.B,
                                            Grade.C, Grade.E, Grade.D, Grade.B,
                                            Grade.E, Grade.A, Grade.FAIL,
                                            Grade.FAIL, Grade.FAIL, Grade.FAIL,
                                            Grade.FAIL, Grade.FAIL);
        double expectedAverage = 3.0;
        GradeBook gpa = new GradeBook(grades);
        assertEquals(expectedAverage, gpa.getGpa());
    }
}
