package inf101h22.university;

import java.util.List;

public class Student {
    
    public final int id;
    private String name;
    private final GradeBook grades;

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
        this.grades = new GradeBook();
    }

    public Student(int id, String name, List<Grade> gradeList) {
        this.id = id;
        this.name = name;
        this.grades = new GradeBook(gradeList);
    }

    public GradeBook getGrades() {
        return this.grades;
    }
    
    public String getName() {
        return this.name;
    }
}
