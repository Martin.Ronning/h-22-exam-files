package inf101h22.treedrawer.view;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;

/**
 * Draws the head-up display displaying a message
 */
public class HeadUpDisplay extends JComponent {

    public HeadUpDisplay() {
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

        this.add(Box.createHorizontalGlue());
        this.add(new JLabel("TODO"));
        this.add(Box.createHorizontalGlue());
    }

}
