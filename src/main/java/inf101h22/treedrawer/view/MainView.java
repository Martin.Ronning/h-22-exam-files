package inf101h22.treedrawer.view;

import java.awt.BorderLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

/**
 * The main class of the view.
 * Responsible for drawing the entire application.
 */
public class MainView extends JPanel {
    private static final int GAP = 10;

    private HeadUpDisplay top;
    private JLabel middle;
    private ButtonsPanel bottom;

    public MainView() {
        this.setLayout(new BorderLayout(GAP, GAP));
        this.setBorder(new EmptyBorder(GAP, GAP, GAP, GAP));

        this.top = new HeadUpDisplay();
        this.middle = new JLabel("TODO");
        this.bottom = new ButtonsPanel();

        this.add(this.top, BorderLayout.NORTH);
        this.add(this.middle, BorderLayout.CENTER);
        this.add(this.bottom, BorderLayout.SOUTH);
    }
    
}
