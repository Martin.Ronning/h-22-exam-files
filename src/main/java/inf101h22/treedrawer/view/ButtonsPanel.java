package inf101h22.treedrawer.view;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;

/**
 * Draws the panel containing the reset button
 */
public class ButtonsPanel extends JComponent {

    public ButtonsPanel() {
        JButton reset = new JButton("Reset");

        // Panel of buttons
        this.setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        this.add(Box.createHorizontalGlue());
        this.add(reset);
        this.add(Box.createHorizontalGlue());
    }
}
