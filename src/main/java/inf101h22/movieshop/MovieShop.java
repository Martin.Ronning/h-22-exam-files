package inf101h22.movieshop;

import java.util.ArrayList;
import java.util.List;

class Movie {
    public final String title;
    public final String director;

    public Movie(String title, String director) {
        this.title = title;
        this.director = director;
    }

    @Override
    public String toString() {
        return "\"" + title + "\" by " + director;
    }
}

public class MovieShop {
    
    List<Movie> movies;

    public MovieShop() {
        movies = new ArrayList<>();
        movies.add(new Movie("Harry Potter", "C. Columbus"));
        movies.add(new Movie("Avatar", "D. Cameron"));
        movies.add(new Movie("Avengers", "J. Whedon"));
        movies.add(new Movie("The Prestige", "C. Nolan"));
    }

    public boolean hasMovie(Movie movie) {
        return this.movies.contains(movie);
    }

    public static void main(String[] args) {
        MovieShop shop = new MovieShop();
        Movie potter = new Movie("Harry Potter", "C. Columbus");
        boolean hasPotter = shop.hasMovie(potter);

        System.out.println("Does the movie shop have  " + potter + "? " + hasPotter);
    }
}
